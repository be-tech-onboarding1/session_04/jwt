const jwt = require("jsonwebtoken");

// {SECTION} JSON Web TOken
/*
	JWT is a way of securely passing information from the server to the front end ot other parts of the server
	Information is kept secure through a secret code
	only the system knows the secret code that can decode the encrypted information
	Imagine jWT as a gift wrapping service that secures the gift with a lock
	If the wrapper has been tanpered with, JWT also recognizes this and disregards the gift
*/

// Token Creation
const secret = "CourseBookingAPI";

module.exports.createAccessToken = (user) => {

	// payload - data that we store in our token
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	// creation of token
	return jwt.sign(data, secret, {});

};

// Token verification
// receive the gift and open it and verify id the sender is legitimate and gift was not tampered
module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined"){

		// slice method = extract particular section of a tstring wothout modifying the original string
		// Bearer nv457nhgvh43857tf347r5783784
		token = token.slice(7, token.length);

		// nv457nhgvh43857tf347r5783784

		
	}
	else {
		res.send({auth:"failed"})
	}
};